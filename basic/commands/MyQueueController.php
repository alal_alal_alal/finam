<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Elements;
use app\models\WriteJob;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MyQueueController extends Controller
{

    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionAdd($min = 0, $max = 3)
    {
        //Отправка задания в очередь
        for ($i = 0, $k = 0; ($i*($cn =  count(WriteJob::group))) < $max; $i++) {
            foreach (WriteJob::group as $index => $item) {
                $els[] = ['index' => $index, 'config' => [
                    'text' => 'element' . $k++,
                    'group' => $item,
                    'file' => Yii::$app->basePath . '/web/file.txt'
                ]];
            }
        }
        shuffle($els);
        foreach ($els as $index => $item) {
           // foreach (range(1, 4) as $qi => $q) {
                //   создаем несколько потоков

                $ids[] = $id = Yii::$app->{'queue' . ($index+1)%3}->priority($item['index'])->push(new WriteJob($item['config']));
          //  }
        }

        return ExitCode::OK;
    }

    public function actionTest()
    {
        $el = new Elements([
            'id' => null,
            'text' => 123,
            'text2' => 'kkk',
            'mictotime' => microtime(),
            'timestamp' => (new \DateTime())->format(\DateTime::ATOM),
        ]);
        //   $queue->isDone();
        var_dump($el);
        //   file_put_contents($this->file, print_r($el, 1).PHP_EOL,  FILE_APPEND);
        $ok = $el->save(false);
        $ok = $el->save(false);
        return $ok;

    }

    public function actionStat()
    {
        $q = 'SELECT MINUTE(TIMEDIFF(  
     (SELECT max(`timestamp`) FROM `elements`),
     (SELECT min(`timestamp`) FROM `elements`)
)) as  minute , (SELECT  COUNT(*) FROM  `elements`) as  count';
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($q);

        $result = $command->queryAll();
        echo " минут " . $result[0]["minute"] . PHP_EOL . "  задач " . $result[0]["count"] . PHP_EOL;
        return $result;

    }
}
