<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log',
        'queue','queue1','queue2','queue3', // The component registers its own console commands
    ],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'queue' => [
            'db' => $db,
            'mutex' => [
                'class' => 'yii\mutex\MysqlMutex',
            ],
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            // Other driver options
        ],        'queue1' => [
            'db' => $db,
            'mutex' => [
                'class' => 'yii\mutex\MysqlMutex',
            ],
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            // Other driver options
        ],        'queue2' => [
            'strictJobType' =>false,
            'db' => $db,
            'mutex' => [
                'class' => 'yii\mutex\MysqlMutex',
            ],
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            // Other driver options
        ],        'queue3' => [
            'strictJobType' =>false,
            'db' => $db,
            'mutex' => [
                'class' => 'yii\mutex\MysqlMutex',
            ],
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            // Other driver options
        ],        'queue4' => [
            'db' => $db,
            'strictJobType' =>false,
            'mutex' => [
                'class' => 'yii\mutex\MysqlMutex',
            ],
            'class' => \yii\queue\db\Queue::class,
            'as log' => \yii\queue\LogBehavior::class,
            // Other driver options
        ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'app\migrations',
                'yii\queue\db\migrations',
            ],
          //  'migrationPath' => null, // allows to disable not namespaced migration completely
        ],]
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
