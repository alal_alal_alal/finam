<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "elements".
 *
 * @property int $id
 * @property string|null $text
 * @property string|null $text2
 * @property int $mictotime
 * @property string $timestamp
 */
class Elements extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'elements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mictotime'], 'required'],
            [['mictotime'], 'integer'],
            [['timestamp'], 'safe'],
            [['text', 'text2'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'text2' => 'Text2',
            'mictotime' => 'Mictotime',
            'timestamp' => 'Timestamp',
        ];
    }
}
