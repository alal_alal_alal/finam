# finam
##### для первоначальной настройки 
 ```
git clone 
 composer update 
yii init 
 yii migrate init 
 yii migrate migrationPath путь к \basic\vendor\yiisoft\yii2-queue\src\drivers\db\migrations
 mysql-uroot -p <shema.sql
```
##### создание элементов в очереди  
```
yii my-queue/add 1 30
```
##### запуск очереди 
```
yii queue/run
```
##### очистка очереди 
```
yii queue/clear
```
##### просмотр статистики
```
yii my-queue/stat
```
запускать обработку  можно в несколько потоков  через  `queue1` -  `queue3`
 скрипт создает задания которые добавляются в несколько очередей в случайном порядке в очереди обрабатываются по группам. выполнение задания представляет собой создание строчки в таблице `elements`  и  происходит  в  `\app\models\WriteJob::execute`
 
 
